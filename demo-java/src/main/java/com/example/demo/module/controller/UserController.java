package com.example.demo.module.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.example.demo.module.dto.UserDTO;
import com.example.demo.module.service.UserService;

@Controller
@RequestMapping("/api")
public class UserController {

	@Autowired
	UserService userService;


	@GetMapping("/user/list")
	private ResponseEntity<?> getUserdetailsList() {

	
		List<UserDTO> obj = userService.getUserdetailsList();

		return new ResponseEntity<>(obj, HttpStatus.OK);
	}
	
	@GetMapping("/user/detail")
	private ResponseEntity<?> getUserdetails(@RequestParam(name = "id") Integer id) {

		System.err.println("user details");

		UserDTO obj = userService.getUserdetails(id);

		if (obj == null) {
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		}

		return new ResponseEntity<>(obj, HttpStatus.OK);
	}

	@PostMapping("/user/create")
	private ResponseEntity<?> newUser(@RequestBody UserDTO userDTO) {

		Integer id = userService.newUser(userDTO);
			return new ResponseEntity<>(id,HttpStatus.OK);
		
	}

	@PutMapping("/user/update")
	private ResponseEntity<?> updateUser(@RequestBody UserDTO userDTO) {

		Boolean status = userService.updateUser(userDTO);

		if (status) {
			return new ResponseEntity<>(HttpStatus.CONFLICT);
		} else {

			return new ResponseEntity<>(HttpStatus.OK);
		}
	}

	@DeleteMapping("/user/delete")
	private ResponseEntity<?> deleteUser(@RequestParam(name = "id") Integer id) {

		userService.deleteUser(id);

		return new ResponseEntity<>(HttpStatus.OK);

	}

}
